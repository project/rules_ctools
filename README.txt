Rules ctools allows embedding Rules components as content panes in Panels (and
other modules using the CTools content panes). The parameters for the Rules
components can either entered manually by end-users in a form, or set by CTools
contexts.

Furthermore Rules ctools allows to use any rules condition component as access
check for content panes (panels, mini panels and other modules that using
ctools panes.

This module was inspires by Rules panes. Actually much of the code used by
Rules ctools comes (was copied) from Rules panes (with little modifications).

So why not just integrate the access feature to Rules Panes? I think it would
be confusing because the name Rules Panes tells that the module provides
content panes (and not access plugins). So I decided to create a new module
based on the code from Rules Panes.

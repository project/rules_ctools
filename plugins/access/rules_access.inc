<?php

/**
 * @file
 * Plugin definition for a ctools access plugin.
 *
 * This plugin allows it to use rules condition components as access check.
 */

$plugin = array(
  'title' => t("Rules"),
  'description' => t('Control access executing a rules condition component'),
  'callback' => 'rules_ctools_access_ctools_access_check',
  'settings form' => 'rules_ctools_access_ctools_access_settings',
  'summary' => 'rules_ctools_access_ctools_access_summary',
  'defaults' => array('exists' => TRUE),
  'get child' => 'rules_ctools_access_ctools_access_get_child',
  'get children' => 'rules_ctools_access_ctools_access_get_children',
);

/**
 * Get child callback.
 */
function rules_ctools_access_ctools_access_get_child($plugin, $parent, $child) {
  $plugins = rules_ctools_access_ctools_access_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

/**
 * Get children callback.
 */
function rules_ctools_access_ctools_access_get_children($plugin, $parent) {
  $components = rules_get_components(FALSE, 'condition', array('active' => 1));

  $plugins = array();

  foreach ($components as $name => $component) {
    $plugin['title'] = 'Rules: ' . $component->label;
    $plugin['keyword'] = $name;
    $plugin['name'] = $parent . ':' . $name;
    $plugin['subtype'] = $name;
    $plugin['required context'] = array();

    $parameters = $component->parameterInfo();
    foreach ($parameters as $key => $parameter_info) {
      $ctools_name = rules_ctools_convert_rules2ctools($parameter_info['type']);
      $plugin['required context'][] = new ctools_context_required(t($parameter_info['label']), $ctools_name, $key);
    }

    $plugins[$plugin['name']] = $plugin;
  }

  return $plugins;
}

/**
 * Access settings form.
 */
function rules_ctools_access_ctools_access_settings($form, &$form_state, $conf) {
  return $form;
}

/**
 * Settings summary.
 */
function rules_ctools_access_ctools_access_summary() {
  return 'Rules condition returns TRUE';
}

/**
 * Access check callback.
 */
function rules_ctools_access_ctools_access_check($conf, $contexts, $plugin) {
  $component = rules_config_load($plugin['subtype']);
  $element = rules_plugin_factory('condition', 'component_' . $component->name);

  foreach ($contexts as $key => $context) {
    list(, $rules_name) = $plugin['required context'][$key]->keywords;
    if (is_array($context->type)) {
      $type = end($context->type);
      $wrapper = entity_metadata_wrapper(rules_ctools_convert_ctools2rules($type), $context->data);
      $parameter = $wrapper->getIdentifier();
    }
    else {
      $parameter = (string) $context->data;
    }

    $element->settings[$rules_name] = $parameter;
  }

  return $component->executeByArgs($element->settings);
}
